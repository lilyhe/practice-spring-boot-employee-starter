package com.thoughtworks.springbootemployee.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RequestMapping("/employees")
@RestController
public class EmployeeController {
    private final List<Employee> employees = new ArrayList<>();

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee creatEmployee(@RequestBody Employee employee) {
        employee.setId(generateId());
        this.employees.add(employee);
        return employee;
    };

    public Long generateId() {
        return employees.stream()
                .max(Comparator.comparing(Employee::getId))
                .map(employee -> employee.getId() + 1)
                .orElse(1L);
    }

    @GetMapping(params = {"gender"})
    public List<Employee> getEmployeesByGender(@RequestParam String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public Employee getEmployeeByIdApi(@PathVariable Long id) {
        return getEmployeeById(id);
    }

    @GetMapping
    public List<Employee> getAllEmployees() {
        return employees;
    }

    @PutMapping("/{id}")
    public Employee updateEmployeeAgeSalary(@PathVariable("id") Long id, @RequestBody Employee employee) {
        Employee updateEmployee = getEmployeeById(id);
        updateEmployee.setAge(employee.getAge());
        updateEmployee.setSalary(employee.getSalary());
        return updateEmployee;
    }

    public Employee getEmployeeById(Long id) {
        return employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteEmployeeById(@PathVariable Long id) {
        employees.remove(getEmployeeById(id));
    }

    @GetMapping(params = {"page", "size"})
    public List<Employee> getEmployeesByPage(@RequestParam Integer page, @RequestParam Integer size) {
        if (page < 0 || size <0) return new ArrayList<>();
        else return employees.stream()
                .skip((long)(page - 1) * size)
                .limit(size)
                .collect(Collectors.toList());
    }
}
